package com.calubsama.recicle_view;

public class ListData {
    private String name= "";
    private String gender= "";
    private int calories= 0;
    private int image= 0;

    public ListData(String name, String gender, int calories, int image){
        this.name = name;
        this.gender = gender;
        this.calories = calories;
        this.image = image;

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}


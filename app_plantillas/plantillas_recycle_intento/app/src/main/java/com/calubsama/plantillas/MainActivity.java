package com.calubsama.plantillas;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), IntentosActivity.class);
                startActivity(i);

            }
        });

        ListData[] listData = new ListData[]{
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo)

        };
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recView);

        ListAdapter listAdapter = new ListAdapter(listData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(listAdapter);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings ) {
            return true;
        }
        if (id == R.id.action_search) {
            Toast.makeText(this,"Texto pro",Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
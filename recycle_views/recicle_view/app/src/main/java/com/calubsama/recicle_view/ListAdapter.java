
package com.calubsama.recicle_view;

import android.text.style.AlignmentSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private ListData[] listData;
    public ListAdapter(ListData[] listData){
        this.listData = listData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View productsItem = layoutInflater.inflate(R.layout.product_item,parent, false);
        ViewHolder viewHolder = new ViewHolder(productsItem);
        return viewHolder;
    }

    @Override
    public  int getItemCount(){
        return listData.length;
    }
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position){
        final ListData nlistData = listData[position];
        viewHolder.textView.setText(listData[position].getName());
        viewHolder.imageView.setImageResource(listData[position].getImage());
        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"Se seleccionó el el elemento "+nlistData.getName(),Toast.LENGTH_LONG).show();
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView ;
        public TextView textView;
        public LinearLayout linearLayout;


        public ViewHolder(View view){
            super(view);
            this.imageView = (ImageView)view.findViewById(R.id.imageView);
            this.textView = (TextView)view.findViewById(R.id.product_name);
            this.textView = (TextView)view.findViewById(R.id.product_calorie);
            this.linearLayout = (LinearLayout)view.findViewById(R.id.layout_products);
        }
    }


}

package com.calubsama.recicle_view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListData[] listData = new ListData[]{
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana roja","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Jugo de Naranja","Embotellados",120,R.drawable.jugo)

        };
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recView);

        ListAdapter listAdapter = new ListAdapter(listData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(listAdapter);
    }
}
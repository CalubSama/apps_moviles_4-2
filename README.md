# Practicas de applicaciones moviles. :monkey:
Estructura del projecto :zap:
```mermaid
graph TD;
  Documents-->First_try_app;
  Documents-->Layouts_basicos;
  Documents-->Recycle_views;
  Documents-->App_plantillas
  Documents-->Kotlin
  Layouts_basicos-->Layout;
  Layouts_basicos-->Layout_login;
  Layouts_basicos-->Layuot_Gta;
  Recycle_views-->Recycle_view_fail;
  Recycle_views-->Recycle_view;
  App_plantillas-->Plantillas;
  App_plantillas-->Plantillas_intento;
  App_plantillas-->Plantillas_recyclerView;
  Kotlin-->Plantilla_kotlin
  Kotlin-->Pokedex
```